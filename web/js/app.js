


(function (window, document, $, Mr, undef) {
    
    var AR;
    
    window.log = function(){
        log.history = log.history || [];   // store logs to an array for reference
        log.history.push(arguments);
        if(window.console){
            console.log( Array.prototype.slice.call(arguments) );
        }
    };
    
    AR = window.AR || {};

    AR = {
        $container : ''
    };

    AR.$container = $('#container');


    //refresha constructor function
    function refresher($element) {
        var me = this;
        me.$refresher = $element;
        me.$form = me.$refresher.find('form');
        me.$urlField = me.$refresher.find('input[name="refreshUrl"]');
        me.$rateField = me.$refresher.find('input[name="refreshInterval"]');
        me.$startBtn = me.$refresher.find('.refresher-btn');
        me.$stopBtn = me.$refresher.find('.stop-btn');
        me.$removeBtn = me.$refresher.find('.remove-btn');
        me.$iFrame = me.$refresher.find('.refresher-iframe');

        me.$startBtn.on('click', function () {
            me.startRefresher()
        });
        me.$removeBtn.on('click', function () {
            me.suicide()
        });
    }

    refresher.prototype.getUrl = function () {
        return this.$urlField.val();
    };

    refresher.prototype.getRate = function () {
        return this.$rateField.val();
    };

    refresher.prototype.convertMilliSecond = function () {
        return this.$rateField.val();
    };

    refresher.prototype.suicide = function () {
        delete this;
    };



    refresher.prototype.startRefresher = function () {
        var me = this,
            url = this.getUrl(),
            rateInSecond = parseInt(this.getRate());

        if(url && rateInSecond) {
            var rateInMillisecond = rateInSecond * 1000;

            me.$refresher.addClass('active');

            AR.$container.removeClass('intro');
    
            //me.$form.addClass('hide');
            me.$iFrame.removeClass('hide');

            me.$iFrame.attr('src', url);
            me.timer = setInterval(function () {
                me.$iFrame.attr('src', url);
            }, rateInMillisecond);
            
        };
        
    };
    
    
    (function (){
        var refresherClass   = 'refresher-wrapper',
            $refresher       = $('.' + refresherClass);

        $refresher.each( function (index, wrapperElement) {
            var thisRefresher = new refresher($(this));
        });
        
    }());
    
    

}(this, this.document, this.jQuery, Modernizr));


